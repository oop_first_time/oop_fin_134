/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.tanitha.fruitshop;

import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author USER
 */
public class Menu extends javax.swing.JFrame implements MouseListener {

    /**
     * Creates new form Menu
     */
    public Menu() {
        initComponents();
        panelStb.addMouseListener(this);
        panelLemon.addMouseListener(this);
        panelPear.addMouseListener(this);
        panelGreenLemon.addMouseListener(this);
        panelPeach.addMouseListener(this);
        panelBanana.addMouseListener(this);
        panelOrange.addMouseListener(this);
        panelKiwi.addMouseListener(this);
        panelCherry.addMouseListener(this);
        panelPineapple.addMouseListener(this);

    }

    DefaultTableModel model = new DefaultTableModel();

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        panelStb = new javax.swing.JPanel();
        lblStb = new javax.swing.JLabel();
        imgStb = new javax.swing.JLabel();
        priceStb = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        panelLemon = new javax.swing.JPanel();
        lblLemon = new javax.swing.JLabel();
        priceLemon = new javax.swing.JLabel();
        imgLemon = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        panelPear = new javax.swing.JPanel();
        lblPear = new javax.swing.JLabel();
        pricePear = new javax.swing.JLabel();
        imgPear = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        panelGreenLemon = new javax.swing.JPanel();
        lblGreenLemon = new javax.swing.JLabel();
        priceGreenLemon = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        imgGreenLemon = new javax.swing.JLabel();
        panelBanana = new javax.swing.JPanel();
        lblBanana = new javax.swing.JLabel();
        priceBanana = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        imgBanana = new javax.swing.JLabel();
        panelOrange = new javax.swing.JPanel();
        lblOrange = new javax.swing.JLabel();
        priceOrange = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        imgOrange = new javax.swing.JLabel();
        panelKiwi = new javax.swing.JPanel();
        lblKiwi = new javax.swing.JLabel();
        priceKiwi = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        imgKiwi = new javax.swing.JLabel();
        panelCherry = new javax.swing.JPanel();
        lblCherry = new javax.swing.JLabel();
        priceCherry = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        imgCherry = new javax.swing.JLabel();
        panelPeach = new javax.swing.JPanel();
        lblPeach = new javax.swing.JLabel();
        pricePeach = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        imgPeach = new javax.swing.JLabel();
        panelPineapple = new javax.swing.JPanel();
        lblPineapple = new javax.swing.JLabel();
        pricePineapple = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        imgPineapple2 = new javax.swing.JLabel();
        imgPineapple1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        lblTotal = new javax.swing.JLabel();
        txtSum = new javax.swing.JTextField();
        lblTotal1 = new javax.swing.JLabel();
        btnOrder = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 204, 51));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\stdy.png")); // NOI18N

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Fahkwang Medium", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Stardew Valley ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(775, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(7, 7, 7))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setOpaque(false);

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setHorizontalScrollBar(null);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setAutoscrolls(true);

        panelStb.setBackground(new java.awt.Color(232, 232, 232));
        panelStb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelStb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelStbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelStbMouseExited(evt);
            }
        });

        lblStb.setBackground(new java.awt.Color(0, 0, 0));
        lblStb.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblStb.setForeground(new java.awt.Color(0, 0, 0));
        lblStb.setText("Strawberry");

        imgStb.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\starwberry.png")); // NOI18N
        imgStb.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        priceStb.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceStb.setForeground(new java.awt.Color(102, 102, 102));
        priceStb.setText("฿ 350");

        javax.swing.GroupLayout panelStbLayout = new javax.swing.GroupLayout(panelStb);
        panelStb.setLayout(panelStbLayout);
        panelStbLayout.setHorizontalGroup(
            panelStbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStbLayout.createSequentialGroup()
                        .addComponent(imgStb, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(panelStbLayout.createSequentialGroup()
                        .addGroup(panelStbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(priceStb, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(panelStbLayout.createSequentialGroup()
                .addComponent(lblStb, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelStbLayout.setVerticalGroup(
            panelStbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStbLayout.createSequentialGroup()
                .addComponent(lblStb)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgStb, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(priceStb, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panelLemon.setBackground(new java.awt.Color(232, 232, 232));
        panelLemon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelLemon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelLemonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelLemonMouseExited(evt);
            }
        });

        lblLemon.setBackground(new java.awt.Color(0, 0, 0));
        lblLemon.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblLemon.setForeground(new java.awt.Color(0, 0, 0));
        lblLemon.setText("Lemon");

        priceLemon.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceLemon.setForeground(new java.awt.Color(102, 102, 102));
        priceLemon.setText("฿ 57");

        imgLemon.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\lemon.png")); // NOI18N
        imgLemon.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelLemonLayout = new javax.swing.GroupLayout(panelLemon);
        panelLemon.setLayout(panelLemonLayout);
        panelLemonLayout.setHorizontalGroup(
            panelLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLemonLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panelLemonLayout.createSequentialGroup()
                .addGroup(panelLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLemonLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(imgLemon))
                    .addGroup(panelLemonLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(priceLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        panelLemonLayout.setVerticalGroup(
            panelLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLemonLayout.createSequentialGroup()
                .addComponent(lblLemon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(priceLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        panelPear.setBackground(new java.awt.Color(232, 232, 232));
        panelPear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelPear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelPearMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelPearMouseExited(evt);
            }
        });

        lblPear.setBackground(new java.awt.Color(0, 0, 0));
        lblPear.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblPear.setForeground(new java.awt.Color(0, 0, 0));
        lblPear.setText("Pear");

        pricePear.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        pricePear.setForeground(new java.awt.Color(102, 102, 102));
        pricePear.setText(" ฿ 45");

        imgPear.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\pear.png")); // NOI18N
        imgPear.setText("jLabel2");
        imgPear.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelPearLayout = new javax.swing.GroupLayout(panelPear);
        panelPear.setLayout(panelPearLayout);
        panelPearLayout.setHorizontalGroup(
            panelPearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPearLayout.createSequentialGroup()
                .addGroup(panelPearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPearLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pricePear, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPearLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(imgPear, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 36, Short.MAX_VALUE))
            .addGroup(panelPearLayout.createSequentialGroup()
                .addGroup(panelPearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPear, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelPearLayout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelPearLayout.setVerticalGroup(
            panelPearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPearLayout.createSequentialGroup()
                .addComponent(lblPear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgPear, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(pricePear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        panelGreenLemon.setBackground(new java.awt.Color(232, 232, 232));
        panelGreenLemon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelGreenLemon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelGreenLemonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelGreenLemonMouseExited(evt);
            }
        });

        lblGreenLemon.setBackground(new java.awt.Color(0, 0, 0));
        lblGreenLemon.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblGreenLemon.setForeground(new java.awt.Color(0, 0, 0));
        lblGreenLemon.setText("Green-Lemon");

        priceGreenLemon.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceGreenLemon.setForeground(new java.awt.Color(102, 102, 102));
        priceGreenLemon.setText(" ฿ 20");

        imgGreenLemon.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\green-lemon.pmg.png")); // NOI18N
        imgGreenLemon.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelGreenLemonLayout = new javax.swing.GroupLayout(panelGreenLemon);
        panelGreenLemon.setLayout(panelGreenLemonLayout);
        panelGreenLemonLayout.setHorizontalGroup(
            panelGreenLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGreenLemonLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(71, Short.MAX_VALUE))
            .addGroup(panelGreenLemonLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(imgGreenLemon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelGreenLemonLayout.createSequentialGroup()
                .addGroup(panelGreenLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGreenLemonLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(priceGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelGreenLemonLayout.setVerticalGroup(
            panelGreenLemonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGreenLemonLayout.createSequentialGroup()
                .addComponent(lblGreenLemon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imgGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(priceGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        panelBanana.setBackground(new java.awt.Color(232, 232, 232));
        panelBanana.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelBanana.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelBananaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelBananaMouseExited(evt);
            }
        });

        lblBanana.setBackground(new java.awt.Color(0, 0, 0));
        lblBanana.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblBanana.setForeground(new java.awt.Color(0, 0, 0));
        lblBanana.setText("Banana");

        priceBanana.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceBanana.setForeground(new java.awt.Color(102, 102, 102));
        priceBanana.setText(" ฿  20");

        imgBanana.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\banana.png")); // NOI18N
        imgBanana.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelBananaLayout = new javax.swing.GroupLayout(panelBanana);
        panelBanana.setLayout(panelBananaLayout);
        panelBananaLayout.setHorizontalGroup(
            panelBananaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBananaLayout.createSequentialGroup()
                .addGroup(panelBananaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblBanana, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelBananaLayout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelBananaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBananaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBananaLayout.createSequentialGroup()
                        .addComponent(priceBanana, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(imgBanana, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)))
        );
        panelBananaLayout.setVerticalGroup(
            panelBananaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBananaLayout.createSequentialGroup()
                .addComponent(lblBanana)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imgBanana, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(priceBanana, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelOrange.setBackground(new java.awt.Color(232, 232, 232));
        panelOrange.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelOrange.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelOrangeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelOrangeMouseExited(evt);
            }
        });

        lblOrange.setBackground(new java.awt.Color(0, 0, 0));
        lblOrange.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblOrange.setForeground(new java.awt.Color(0, 0, 0));
        lblOrange.setText("Orange");

        priceOrange.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceOrange.setForeground(new java.awt.Color(102, 102, 102));
        priceOrange.setText(" ฿ 45");

        imgOrange.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\org.png")); // NOI18N
        imgOrange.setText("jLabel2");
        imgOrange.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelOrangeLayout = new javax.swing.GroupLayout(panelOrange);
        panelOrange.setLayout(panelOrangeLayout);
        panelOrangeLayout.setHorizontalGroup(
            panelOrangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOrangeLayout.createSequentialGroup()
                .addGroup(panelOrangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelOrangeLayout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelOrangeLayout.createSequentialGroup()
                .addGroup(panelOrangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelOrangeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(priceOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelOrangeLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(imgOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 36, Short.MAX_VALUE))
        );
        panelOrangeLayout.setVerticalGroup(
            panelOrangeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOrangeLayout.createSequentialGroup()
                .addComponent(lblOrange)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(priceOrange, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelKiwi.setBackground(new java.awt.Color(232, 232, 232));
        panelKiwi.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelKiwi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelKiwiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelKiwiMouseExited(evt);
            }
        });

        lblKiwi.setBackground(new java.awt.Color(0, 0, 0));
        lblKiwi.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblKiwi.setForeground(new java.awt.Color(0, 0, 0));
        lblKiwi.setText("Kiwi");

        priceKiwi.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceKiwi.setForeground(new java.awt.Color(102, 102, 102));
        priceKiwi.setText(" ฿ 85");

        imgKiwi.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\kiwi.png")); // NOI18N
        imgKiwi.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelKiwiLayout = new javax.swing.GroupLayout(panelKiwi);
        panelKiwi.setLayout(panelKiwiLayout);
        panelKiwiLayout.setHorizontalGroup(
            panelKiwiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelKiwiLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelKiwiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelKiwiLayout.createSequentialGroup()
                        .addComponent(lblKiwi, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelKiwiLayout.createSequentialGroup()
                        .addComponent(priceKiwi, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(panelKiwiLayout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelKiwiLayout.createSequentialGroup()
                .addContainerGap(50, Short.MAX_VALUE)
                .addComponent(imgKiwi, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        panelKiwiLayout.setVerticalGroup(
            panelKiwiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelKiwiLayout.createSequentialGroup()
                .addComponent(lblKiwi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgKiwi)
                .addGap(18, 18, 18)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(priceKiwi, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCherry.setBackground(new java.awt.Color(232, 232, 232));
        panelCherry.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelCherry.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelCherryMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelCherryMouseExited(evt);
            }
        });

        lblCherry.setBackground(new java.awt.Color(0, 0, 0));
        lblCherry.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblCherry.setForeground(new java.awt.Color(0, 0, 0));
        lblCherry.setText("Cherry");

        priceCherry.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        priceCherry.setForeground(new java.awt.Color(102, 102, 102));
        priceCherry.setText(" ฿ 95");

        imgCherry.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\cherry.png")); // NOI18N
        imgCherry.setText("jLabel2");
        imgCherry.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelCherryLayout = new javax.swing.GroupLayout(panelCherry);
        panelCherry.setLayout(panelCherryLayout);
        panelCherryLayout.setHorizontalGroup(
            panelCherryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCherryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCherryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCherryLayout.createSequentialGroup()
                        .addComponent(lblCherry, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(87, Short.MAX_VALUE))
                    .addGroup(panelCherryLayout.createSequentialGroup()
                        .addComponent(priceCherry, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(panelCherryLayout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCherryLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(imgCherry, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        panelCherryLayout.setVerticalGroup(
            panelCherryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCherryLayout.createSequentialGroup()
                .addComponent(lblCherry)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgCherry, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(priceCherry, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        panelPeach.setBackground(new java.awt.Color(232, 232, 232));
        panelPeach.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelPeach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelPeachMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelPeachMouseExited(evt);
            }
        });

        lblPeach.setBackground(new java.awt.Color(0, 0, 0));
        lblPeach.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblPeach.setForeground(new java.awt.Color(0, 0, 0));
        lblPeach.setText("Peach");

        pricePeach.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        pricePeach.setForeground(new java.awt.Color(102, 102, 102));
        pricePeach.setText(" ฿ 100");

        imgPeach.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\peach.png")); // NOI18N
        imgPeach.setText("jLabel2");
        imgPeach.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelPeachLayout = new javax.swing.GroupLayout(panelPeach);
        panelPeach.setLayout(panelPeachLayout);
        panelPeachLayout.setHorizontalGroup(
            panelPeachLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPeachLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPeachLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPeachLayout.createSequentialGroup()
                        .addComponent(lblPeach, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(87, Short.MAX_VALUE))
                    .addGroup(panelPeachLayout.createSequentialGroup()
                        .addComponent(pricePeach, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(panelPeachLayout.createSequentialGroup()
                .addGroup(panelPeachLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPeachLayout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPeachLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(imgPeach, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelPeachLayout.setVerticalGroup(
            panelPeachLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPeachLayout.createSequentialGroup()
                .addComponent(lblPeach)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imgPeach, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pricePeach, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        panelPineapple.setBackground(new java.awt.Color(232, 232, 232));
        panelPineapple.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        panelPineapple.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelPineappleMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                panelPineappleMouseExited(evt);
            }
        });

        lblPineapple.setBackground(new java.awt.Color(0, 0, 0));
        lblPineapple.setFont(new java.awt.Font("Fahkwang Medium", 1, 18)); // NOI18N
        lblPineapple.setForeground(new java.awt.Color(0, 0, 0));
        lblPineapple.setText("Pineapple");

        pricePineapple.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        pricePineapple.setForeground(new java.awt.Color(102, 102, 102));
        pricePineapple.setText(" ฿ 60");

        imgPineapple2.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\pineapple.png")); // NOI18N
        imgPineapple2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        imgPineapple1.setIcon(new javax.swing.ImageIcon("C:\\Users\\USER\\Desktop\\Project Fin\\image\\pineapple.png")); // NOI18N
        imgPineapple1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelPineappleLayout = new javax.swing.GroupLayout(panelPineapple);
        panelPineapple.setLayout(panelPineappleLayout);
        panelPineappleLayout.setHorizontalGroup(
            panelPineappleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPineappleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPineappleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPineappleLayout.createSequentialGroup()
                        .addComponent(lblPineapple, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPineappleLayout.createSequentialGroup()
                        .addGap(0, 8, Short.MAX_VALUE)
                        .addGroup(panelPineappleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPineappleLayout.createSequentialGroup()
                                .addComponent(imgPineapple1, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(imgPineapple2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPineappleLayout.createSequentialGroup()
                                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(80, 80, 80))))
                    .addGroup(panelPineappleLayout.createSequentialGroup()
                        .addComponent(pricePineapple, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        panelPineappleLayout.setVerticalGroup(
            panelPineappleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPineappleLayout.createSequentialGroup()
                .addComponent(lblPineapple)
                .addGroup(panelPineappleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPineappleLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(imgPineapple1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPineappleLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(imgPineapple2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pricePineapple, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable.setBackground(new java.awt.Color(78, 78, 78));
        jTable.setFont(new java.awt.Font("Fahkwang Light", 0, 14)); // NOI18N
        jTable.setForeground(new java.awt.Color(255, 255, 255));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item", "Price", " Quantity", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable);

        lblTotal.setFont(new java.awt.Font("Fahkwang Medium", 1, 24)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(138, 221, 103));
        lblTotal.setText("BAHT");

        txtSum.setBackground(new java.awt.Color(235, 235, 235));
        txtSum.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        txtSum.setForeground(new java.awt.Color(51, 51, 51));

        lblTotal1.setFont(new java.awt.Font("Fahkwang Medium", 1, 24)); // NOI18N
        lblTotal1.setForeground(new java.awt.Color(138, 221, 103));
        lblTotal1.setText("TOTAL:");

        btnOrder.setBackground(new java.awt.Color(227, 73, 73));
        btnOrder.setFont(new java.awt.Font("Fahkwang Medium", 1, 14)); // NOI18N
        btnOrder.setForeground(new java.awt.Color(255, 255, 255));
        btnOrder.setText("Order");
        btnOrder.setBorder(null);
        btnOrder.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(panelStb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(panelLemon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(panelPear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(panelGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(panelPeach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(panelBanana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(panelOrange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(panelKiwi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(panelCherry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addComponent(panelPineapple, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 714, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSum, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(131, 131, 131)
                                .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelGreenLemon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelStb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelLemon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPeach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelBanana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelOrange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelKiwi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCherry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPineapple, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(181, 181, 181)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSum, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 129, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1174, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(59, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void panelStbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelStbMouseEntered
        setPanelColor(panelStb);
    }//GEN-LAST:event_panelStbMouseEntered

    private void panelStbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelStbMouseExited
        resetPanelColor(panelStb);
    }//GEN-LAST:event_panelStbMouseExited

    private void panelLemonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelLemonMouseEntered
        setPanelColor(panelLemon);
    }//GEN-LAST:event_panelLemonMouseEntered

    private void panelLemonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelLemonMouseExited
        resetPanelColor(panelLemon);
    }//GEN-LAST:event_panelLemonMouseExited

    private void panelPearMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPearMouseExited
        resetPanelColor(panelPear);
    }//GEN-LAST:event_panelPearMouseExited

    private void panelPearMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPearMouseEntered
        setPanelColor(panelPear);
    }//GEN-LAST:event_panelPearMouseEntered

    private void panelGreenLemonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelGreenLemonMouseEntered
        setPanelColor(panelGreenLemon);
    }//GEN-LAST:event_panelGreenLemonMouseEntered

    private void panelGreenLemonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelGreenLemonMouseExited
        resetPanelColor(panelGreenLemon);
    }//GEN-LAST:event_panelGreenLemonMouseExited

    private void panelBananaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelBananaMouseEntered
        setPanelColor(panelBanana);
    }//GEN-LAST:event_panelBananaMouseEntered

    private void panelBananaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelBananaMouseExited
        resetPanelColor(panelBanana);
    }//GEN-LAST:event_panelBananaMouseExited

    private void panelOrangeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelOrangeMouseEntered
        setPanelColor(panelOrange);
    }//GEN-LAST:event_panelOrangeMouseEntered

    private void panelOrangeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelOrangeMouseExited
        resetPanelColor(panelOrange);
    }//GEN-LAST:event_panelOrangeMouseExited

    private void panelKiwiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelKiwiMouseEntered
        setPanelColor(panelKiwi);
    }//GEN-LAST:event_panelKiwiMouseEntered

    private void panelKiwiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelKiwiMouseExited
        resetPanelColor(panelKiwi);
    }//GEN-LAST:event_panelKiwiMouseExited

    private void panelCherryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelCherryMouseEntered
        setPanelColor(panelCherry);
    }//GEN-LAST:event_panelCherryMouseEntered

    private void panelCherryMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelCherryMouseExited
        resetPanelColor(panelCherry);
    }//GEN-LAST:event_panelCherryMouseExited

    private void panelPeachMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPeachMouseEntered
        setPanelColor(panelPeach);
    }//GEN-LAST:event_panelPeachMouseEntered

    private void panelPeachMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPeachMouseExited
        resetPanelColor(panelPeach);
    }//GEN-LAST:event_panelPeachMouseExited

    private void panelPineappleMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPineappleMouseEntered
        setPanelColor(panelPineapple);
    }//GEN-LAST:event_panelPineappleMouseEntered

    private void panelPineappleMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPineappleMouseExited
        resetPanelColor(panelPineapple);
    }//GEN-LAST:event_panelPineappleMouseExited

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        OrderFinish odf = new OrderFinish();
        odf.setVisible(true);

    }//GEN-LAST:event_btnOrderActionPerformed

    public void setPanelColor(JPanel panel) {
        panel.setBackground(new java.awt.Color(115, 163, 239));
    }

    public void resetPanelColor(JPanel panel) {
        panel.setBackground(new java.awt.Color(232, 232, 232));
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOrder;
    private javax.swing.JLabel imgBanana;
    private javax.swing.JLabel imgCherry;
    private javax.swing.JLabel imgGreenLemon;
    private javax.swing.JLabel imgKiwi;
    private javax.swing.JLabel imgLemon;
    private javax.swing.JLabel imgOrange;
    private javax.swing.JLabel imgPeach;
    private javax.swing.JLabel imgPear;
    private javax.swing.JLabel imgPineapple1;
    private javax.swing.JLabel imgPineapple2;
    private javax.swing.JLabel imgStb;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTable jTable;
    private javax.swing.JLabel lblBanana;
    private javax.swing.JLabel lblCherry;
    private javax.swing.JLabel lblGreenLemon;
    private javax.swing.JLabel lblKiwi;
    private javax.swing.JLabel lblLemon;
    private javax.swing.JLabel lblOrange;
    private javax.swing.JLabel lblPeach;
    private javax.swing.JLabel lblPear;
    private javax.swing.JLabel lblPineapple;
    private javax.swing.JLabel lblStb;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotal1;
    private javax.swing.JPanel panelBanana;
    private javax.swing.JPanel panelCherry;
    private javax.swing.JPanel panelGreenLemon;
    private javax.swing.JPanel panelKiwi;
    private javax.swing.JPanel panelLemon;
    private javax.swing.JPanel panelOrange;
    private javax.swing.JPanel panelPeach;
    private javax.swing.JPanel panelPear;
    private javax.swing.JPanel panelPineapple;
    private javax.swing.JPanel panelStb;
    private javax.swing.JLabel priceBanana;
    private javax.swing.JLabel priceCherry;
    private javax.swing.JLabel priceGreenLemon;
    private javax.swing.JLabel priceKiwi;
    private javax.swing.JLabel priceLemon;
    private javax.swing.JLabel priceOrange;
    private javax.swing.JLabel pricePeach;
    private javax.swing.JLabel pricePear;
    private javax.swing.JLabel pricePineapple;
    private javax.swing.JLabel priceStb;
    private javax.swing.JTextField txtSum;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getSource() == panelStb) {
            String name = "Strawberry";
            int price = 350;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelLemon) {
            String name = "Lemon";
            int price = 57;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelPear) {
            String name = "Pear";
            int price = 45;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelGreenLemon) {
            String name = "Green-Lemon";
            int price = 20;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelPeach) {
            String name = "Peach";
            int price = 100;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelBanana) {
            String name = "Banana";
            int price = 20;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelOrange) {
            String name = "Orange";
            int price = 45;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelKiwi) {
            String name = "Kiwi";
            int price = 85;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelCherry) {
            String name = "Cherry";
            int price = 95;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        } else if (e.getSource() == panelPineapple) {
            String name = "Pineapple";
            int price = 60;

            int qty = Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity,"));
            int tot = price * qty;

            model = (DefaultTableModel) jTable.getModel();

            model.addRow(new Object[]{
                name,
                price,
                qty,
                tot,});

            int sum = 0;
            for (int i = 0; i < jTable.getRowCount(); i++) {
                sum = sum + Integer.parseInt(jTable.getValueAt(i, 3).toString());
            }
            txtSum.setText(Integer.toString(sum));
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
